import { exec } from "child_process";
import { writeFile, unlink } from "fs";
import { languages } from "../config/app.js";
import { renderCodeTestCase } from "../utils/helper.js";

const runShell = async (cmd, fileName, run_limit_seconds) => {
    return await new Promise((resolve, reject) => {
        let command = `${cmd} ${fileName}`;
        if (cmd === "c") {
            command = `gcc ${fileName} -o ${fileName.replace(
                ".c",
                ".out"
            )} && ${fileName.replace(".c", ".out")}`;
        }
        if (cmd === "java") {
            command = `javac ${fileName} && cd src/runner/temp/ && java Main`;
        }
        exec(
            command,
            { timeout: run_limit_seconds * 1000 },
            (error, stdout, stderr) => {
                if (error) {
                    return resolve({
                        error: stderr,
                    });
                }
                return resolve(stdout.split("\n"));
            }
        );
    });
};

const runCode = async (code, language, functionName, run_limit_seconds, run_limit_memory, test_case, params ) => {
    const dataLang = languages.find((lang) => lang.code === language);
    const { command, extension } = dataLang;

    const resultTestCase = [];

    for (let i = 0; i < test_case.length; i++) {
        const { input, expect, _id, hidden } = test_case[i];
        const pathDir = `src/runner/temp/`;
        const fileName = `${pathDir}${Date.now()}.${extension}`;

        const codeRender = renderCodeTestCase(
            functionName,
            input,
            code,
            language,
            params,
        );

        await writeFile(fileName, codeRender, (err) => {
            if (err) {
                console.log(err);
            }
        });

        const timeStart = Date.now();
        const result = await runShell(command, fileName, run_limit_seconds);
        const timeEnd = Date.now();
        const time = (timeEnd - timeStart) / 1000;

        // check delete file temp
        if (command === "c") {
            const fileBuild = fileName.replace(".c", ".out");
            await unlink(fileBuild, (err) => {
                if (err) {
                    console.log(err);
                }
            });
        } 
        
        if (command === "java") {
            const fileBuild = `${pathDir}Main.class`;
            await unlink(fileBuild, (err) => {
                if (err) {
                    console.log(err);
                }
            });
        }

        await unlink(fileName, (err) => {
            if (err) {
                console.log(err);
            }
        });
        

        // check result run shell
        if (result.error) {
            resultTestCase.push({
                id: _id,
                status: "error",
                pass: false,
                output: result.error,
                expect: expect.trim(),
                time_run: time,
                memory_run: 0,
            });
            continue;
        }

        const memory_run = result[result.length - 1].trim();
        result[0] = String(result[0]);
        if(language === "python3"){
            if(result[0].trim() == "False"){
                result[0] = "false";
            }
            if(result[0].trim() == "True"){
                result[0] = "true";
            }
        }

        result[0] = result[0].replace("[ ", "[");
        result[0] = result[0].replace(" ]", "]");

        if(result[0].trim() == expect.trim() && !hidden && memory_run <= run_limit_memory){
            resultTestCase.push({
                id: _id,
                status: "success",
                pass: true,
                output: result[0].trim(),
                expect: expect.trim(),
                time_run: time,
                memory_run: result[1].trim(),
            });
            continue;
        }

        if(result[0].trim() === expect.trim() && hidden && memory_run <= run_limit_memory){
            resultTestCase.push({
                id: _id,
                status: "success",
                pass: true,
                output: "",
                expect: "",
                time_run: time,
                memory_run: result[1].trim(),
            });
            continue;
        }

        if(result[0].trim() !== expect.trim() && !hidden){
            resultTestCase.push({
                id: _id,
                status: "error",
                pass: false,
                output: result[0].trim(),
                expect: expect.trim(),
                time_run: time,
                memory_run: result[1].trim(),
            });
            continue;
        }

        if(memory_run > run_limit_memory){
            resultTestCase.push({
                id: _id,
                status: "error",
                pass: false,
                output: "Memory Limit Exceeded",
                expect: "",
                time_run: time,
                memory_run: result[1].trim(),
            });
            continue;
        }
        resultTestCase.push({
            id: _id,
            status: "error",
            pass: false,
            output: result[0].trim(),
            expect: "",
            time_run: time,
            memory_run: result[1].trim(),
        });
    }
    return resultTestCase;
};

export { runCode };
