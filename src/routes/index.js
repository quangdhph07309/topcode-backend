import user from './user.js'
import admin from './admin.js'
import client from './client.js'

const router = (app) => {
    app.use('/user', user)
    app.use('/admin', admin)
    app.use('/', client)
}

export default router
