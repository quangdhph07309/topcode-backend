import express from "express";
const router = express.Router();
import { auth, authAdmin } from "../app/middleware/auth.js";
import {
    getListUser,
    getOneUser,
    inActiveUser,
    updateUser,
} from "../app/controllers/admin/user.controller.js";
import {
    getConfig,
    updateConfig,
} from "../app/controllers/admin/config.controller.js";
import {
    getListChallenge,
    updateChallenge,
    acceptChallenge,
    deleteChallenge,
} from "../app/controllers/admin/challenge.controller.js";
import { validateChallenge } from "../utils/validator.js";
import {
    createContest,
    detailContest,
    listContest,
    updateContest,
} from "../app/controllers/admin/contest.controller.js";

router.get("/list-user", auth, authAdmin, getListUser);
router.get("/user/:id", auth, authAdmin, getOneUser);
router.put("/user/:id", auth, authAdmin, updateUser);
router.put("/inactive-user/:id", auth, authAdmin, inActiveUser);

router.get("/config", auth, authAdmin, getConfig);
router.put("/config", auth, authAdmin, updateConfig);

router.get("/challenges", auth, authAdmin, getListChallenge);
router.post(
    "/update-challenge/:id",
    auth,
    authAdmin,
    validateChallenge,
    updateChallenge
);
router.post("/accept-challenge/:id", auth, authAdmin, acceptChallenge);
router.delete("/delete-challenge/:id", auth, authAdmin, deleteChallenge);

router.get("/contests", auth, authAdmin, listContest);
router.post("/contest", auth, authAdmin, createContest);
router.put("/contest/:id", auth, authAdmin, updateContest);
router.get("/contest/:id", auth, authAdmin, detailContest);

export default router;
