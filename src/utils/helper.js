const renderCodeFunction = (nameFunction, params, output_type, lang) => {
    switch (lang) {
        case "javascript":
            return `function ${nameFunction}(${params.map(param => param.name).join(', ')}) {
}`
        case "python3":
            return `def ${nameFunction}(${params.map(param => param.name).join(', ')}) :
`
        case "php":
            return `<?php
function ${nameFunction}(${params.map(param => '$'+param.name).join(', ')}) {
}`
        case "java":
            return `public static ${convertTypeParam(output_type, lang)} ${nameFunction}(${params.map(param => convertTypeParam(param.type, lang)+' '+param.name).join(', ')}) {
}`
        case "c":
            return `chưa hỗ trợ`
        default:
            return `null`;
    }
}

const renderCodeTestCase = (functionName, inputs, code, lang, params) => {
    let functionRender = checkFun(functionName, inputs, lang, params);
    switch(lang) {
        case "javascript":
            return `${code}
            const temp_run_code = ${functionRender};
            console.log(temp_run_code);
            const used = process.memoryUsage().heapUsed / 1024 / 1024;
            console.log(Math.round(used * 100) / 100);`;
        case "php":
            return `${code}
            $temp_run_code = ${functionRender};
            if(is_array($temp_run_code)) {
                print(json_encode($temp_run_code)."\n");
            }else if(is_bool($temp_run_code)) {
                print(($temp_run_code) ? "true\n" : "false\n");
            }else {
                print($temp_run_code."\n");
            }
            echo round(memory_get_peak_usage(true) / 1024 / 1024, 4);`;
        case 'python3':
            return `import resource
${code}
temp_run_code = ${functionRender}
print(temp_run_code);
useMemory = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
print(useMemory/1024)`;
        case 'java':
            return `import java.util.*;
            class Main {
              ${code}
              public static void main(String[] args) {
                System.out.println(${functionRender});
                System.out.println((float)(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())/1024/1024);
              }
            }`;
        case 'c':
            return `#include <stdbool.h>
            #include <sys/resource.h>
            #include <stdio.h>
            #include <stdlib.h>
            #include <string.h>
            ${code}
            int main(void) {
              printf("%d\\n", ${functionName}(${inputs.map(input => input.value).join(', ')}));
              struct rusage r_usage;
              getrusage(RUSAGE_SELF,&r_usage);
              printf("%f",(r_usage.ru_maxrss/1024.0));
              return 0;
            }`;
    }
}

const convertTypeParam = (type, lang, name = '') => {
    switch (lang) {
        case "c":
            switch (Number(type)) {
                case 1:
                    return "int " + name;
                case 2:
                    return "long" + name;
                case 3:
                    return "float" + name;
                case 4:
                    return "bool" + name;
                case 5:
                    return "string" + name;
                default:
                    return "";
            break;
        }
        case "java":
            switch (Number(type)) {
                case 1:
                    return "int";
                case 2:
                    return "long";
                case 3:
                    return "float";
                case 4:
                    return "char";
                case 5:
                    return "String";
                case 6:
                    return "boolean";
                case 7:
                    return "int[]";
                case 8:
                    return "long[]";
                case 9:
                    return "float[]";
                case 10:
                    return "char[]";
                case 11:
                    return "String[]";
                case 12:
                    return "boolean[]";
                case 13:
                    return "int[][]";
                case 14:
                    return "long[][]";
                case 15:
                    return "float[][]";
                case 16:
                    return "char[][]";
                case 17:
                    return "String[][]";
                case 18:
                    return "boolean[][]";
        }
    }
}

const checkFun = (functionName, inputs, lang, params) => {
    let codeRender = '';
    if(lang === 'java') {
        for (let i = 0; i < inputs.length; i++) {
            if(params[i].type == 4 || params[i].type == 5) {
                codeRender += `"${inputs[i].value}",`;
            }else if(params[i].type > 6) {
                codeRender += `${inputs[i].value.replaceAll("[","{").replaceAll("]","}")},`;
            }else {
                codeRender += `${inputs[i].value},`;
            }
        }
        codeRender = codeRender.slice(0, -1);
        return `${functionName}(${codeRender})`;
    }

    if(lang === 'python3') {
        for (let i = 0; i < inputs.length; i++) {
            if(params[i].type == 6) {
                codeRender += `${inputs[i].value.charAt(0).toUpperCase() + inputs[i].value.slice(1)},`;
            }else {
                codeRender += `${inputs[i].value},`;
            }
        }
        codeRender = codeRender.slice(0, -1);
        return `${functionName}(${codeRender})`;
    }

    for (let i = 0; i < inputs.length; i++) {
        if(params[i].type == 4 || params[i].type == 5) {
            codeRender += `"${inputs[i].value}",`;
        }else {
            codeRender += `${inputs[i].value},`;
        }
    }
    codeRender = codeRender.slice(0, -1);
    return `${functionName}(${codeRender})`;
}

export { renderCodeFunction, renderCodeTestCase };