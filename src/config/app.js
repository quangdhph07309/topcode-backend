import nodemailer from 'nodemailer';
import dotenv from "dotenv";
dotenv.config();

const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    secure: true,
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD
    },
    tls: {
        rejectUnauthorized: false
    }
});

const languages = [
    {
        code: 'c',
        name: 'C',
        extension: 'c',
        command: 'c'
    },
    {
        code: 'php',
        name: 'PHP',
        extension: 'php',
        command: 'php'
    },
    {
        code: 'java',
        name: 'Java',
        extension: 'java',
        command: 'java'
    },
    {
        code: 'javascript',
        name: 'Javascript',
        extension: 'js',
        command: 'node'
    },
    {
        code: 'python3',
        name: 'Python 3',
        extension: 'py',
        command: 'python3'
    },
];

const ranks = [
    {
        code: 1,
        name: 'Basic',
        min: 100,
        max: 150
    },
    {
        code: 2,
        name: 'Intermediate',
        min: 160,
        max: 200
    },
    {
        code: 3,
        name: 'Advanced',
        min: 210,
        max: 300
    },
    {
        code: 4,
        name: 'Expert',
        min: 310,
        max: 450
    },
    {
        code: 5,
        name: 'Master',
        min: 460,
        max: 600
    },
    {
        code: 6,
        name: 'Grand Master',
        min: 610,
        max: 1000
    },
]
export { transporter, languages, ranks };
