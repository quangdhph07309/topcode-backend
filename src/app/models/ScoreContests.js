import mongoose from "mongoose";

const Schema = mongoose.Schema;


const scoreContestsSchema = new Schema({
    challenge_id: {
        type: Schema.Types.ObjectId, 
        ref: 'Challenges' 
    },
    user_id: {
        type: Schema.Types.ObjectId, 
        ref: 'User' 
    },
    contest_id : {
        type: Schema.Types.ObjectId,
        ref: 'Contests',
        required: true,
    },
    score: {
        type: Number,
        default: 0,
    },
    total_submit: {
        type: Number,
        default: 0,
    },
},  { timestamps: {}, strict: true });

const ScoreContests = mongoose.model('ScoreContests', scoreContestsSchema)
export default ScoreContests