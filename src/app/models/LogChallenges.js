import mongoose from "mongoose";

const Schema = mongoose.Schema;


const logChallengesSchema = new Schema({
    challenge_id: {
        type: Schema.Types.ObjectId, 
        ref: 'Challenges' 
    },
    user_id: {
        type: Schema.Types.ObjectId, 
        ref: 'User' 
    },
    code_text: {
        type: String,
        default: ""
    },
    language: {
        type: String,
        require: true
    },
},  { timestamps: {}, strict: true });

const LogChallenges = mongoose.model('LogChallenges', logChallengesSchema)
export default LogChallenges