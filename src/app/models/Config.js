import mongoose from "mongoose";

const Schema = mongoose.Schema;


const configSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    logo: {
        type: String,
        required: true,
    },
    banner: [
        { type: String, default: "" }
    ]
});

const Config = mongoose.model('Config', configSchema)
export default Config