import mongoose from "mongoose";

const Schema = mongoose.Schema;

const challengeSchema = new Schema(
    {
        title: {
            type: String,
            required: true,
            trim: true,
        },
        description: {
            type: String,
            required: true,
            trim: true,
        },
        description_en: {
            type: String,
            required: true,
            trim: true,
        },
        suggestion: {
            type: String,
            trim: true,
        },
        suggestion_en: {
            type: String,
            trim: true,
        },
        name_function: {
            type: String,
            required: true,
            trim: true,
        },
        output_type: {
            type: Number,
            required: true,
        },
        params: [
            {
                name: {
                    type: String,
                    required: true,
                    trim: true,
                },
                type: {
                    type: String,
                    required: true,
                    trim: true,
                },
                index: {
                    type: Number,
                    required: true,
                },
            },
        ],
        code_temps: [
            {
                lang: {
                    type: String,
                    required: true,
                    trim: true,
                },
                code: {
                    type: String,
                    required: true,
                    trim: true,
                },
            },
        ],
        score: {
            type: Number,
            required: true,
        },
        rank: {
            type: Number,
            default: 0,
        },
        type: {
            type: Number,
            default: 0,
        },
        status: {
            type: Number,
            default: 0,
        },
        created_by: {
            type: Schema.Types.ObjectId,
            ref: "User",
        },
        run_limit_seconds: {
            type: Number,
            default: 0,
        },
        run_limit_memory: {
            type: Number,
            default: 0,
        },
    },
    { timestamps: {}, strict: true }
);

const Challenges = mongoose.model("Challenges", challengeSchema);
export default Challenges;
