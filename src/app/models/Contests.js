import mongoose from "mongoose";

const Schema = mongoose.Schema;

const contestsSchema = new Schema(
    {
        title: {
            type: String,
            required: true,
        },
        avatar: {
            type: String,
            required: true,
            default: "",
        },
        description: {
            type: String,
            required: true,
        },
        time_start: {
            type: Date,
            required: true,
        },
        password: {
            type: String,
            required: true,
        },
        time_register: {
            type: Date,
            required: true,
        },
        time_register_end: {
            type: Date,
            required: true,
        },
        time_end: {
            type: Date,
            required: true,
        },
        status: {
            type: Number,
            default: 0,
        },
        user_join: [
            {
                type: Schema.Types.ObjectId,
                ref: "User"
            },
        ],
        challenge_join: [
            {
                type: Schema.Types.ObjectId,
                ref: "Challenges",
                required: true,
            },
        ],
        created_by: {
            type: Schema.Types.ObjectId,
            ref: "User",
            required: true,
        },
    },
    { timestamps: {}, strict: true }
);

const Contests = mongoose.model("Contests", contestsSchema);
export default Contests;
