import { validationResult } from "express-validator";
import User from "../models/User.js";

const login = async (req, res, next) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res
                .status(400)
                .json({ status: "error", error: errors.array() });
        }

        const { email, password } = req.body;

        const user = await User.findByCredentials(email, password);

        if (!user) {
            throw new Error("Login failed! Check authentication credentials");
        }

        if(!user.verified){
            throw new Error("User inactive");
        }

        const infoUser = {
            _id: user._id,
            name: user.name,
            email: user.email,
            avatar: user.avatar,
            is_admin: user.is_admin,
            rank: user.rank,
            score: user.score,
        };

        const token = await user.generateAuthToken();
        res.status(200).json({ status: "success", data: { infoUser, token } });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const register = async (req, res, next) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res
                .status(400)
                .json({ status: "error", error: errors.array() });
        }

        const { email, name, password, re_password } = req.body;
        const checkEmail = await User.findOne({ email });

        if (checkEmail) {
            throw new Error("Email has been used");
        }
        if (password !== re_password) {
            throw new Error("Password not match");
        }

        const user = new User({ email, name, password });
        await user.save();
        const token = await user.generateAuthToken();

        res.status(201).json({ user, token });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const profile = async (req, res, next) => {
    res.json(req.user);
};

const updateProfile = async (req, res, next) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res
                .status(400)
                .json({ status: "error", error: errors.array() });
        }
        const data = {
            name: req.body.name,
            avatar: req.body.avatar,
        };
        const profile = await User.findOneAndUpdate(
            { _id: req.user._id },
            data
        );
        res.status(200).json({
            status: "success",
            message: "Profile has been updated",
        });
    } catch (error) {
        res.status(400).json({ status: "error", message: error.message });
    }
};

const logout = async (req, res, next) => {
    try {
        const user = await User.findOne({ _id: req.user._id });

        user.tokens = user.tokens.filter(
            (token) => token != req.token
        );

        await user.save();

        res.status(200).json({ status: "success", message: "Logout success" });
    } catch (error) {
        res.status(400).json({ status: "error", message: error.message });
    }
};

export { login, logout, register, profile, updateProfile };
