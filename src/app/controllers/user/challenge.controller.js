import Challenges from "../../models/Challenges.js";
import Cases from "../../models/Cases.js";
import User from "../../models/User.js";
import { runCode } from "../../../runner/index.js";
import { languages, ranks } from "../../../config/app.js";
import { renderCodeFunction } from "../../../utils/helper.js";
import LogChallenges from "../../models/LogChallenges.js";
import SubmitChallenges from "../../models/SubmitChallenges.js";
import { validationResult } from "express-validator";

const createChallenge = async (req, res) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res
                .status(400)
                .json({ status: "error", error: errors.array() });
        }
        const {
            title,
            description,
            description_en,
            suggestion,
            suggestion_en,
            score,
            run_limit_seconds,
            run_limit_memory,
            test_case,
            output_type,
            rank,
        } = req.body;
        const { name_function } = req.body;
        const { params } = req.body;
        const code_temps = [];

        for (let i = 0; i < languages.length; i++) {
            const { code } = languages[i];
            const codeRender = renderCodeFunction(
                name_function,
                params,
                output_type,
                code
            );
            code_temps.push({
                lang: code,
                code: codeRender,
            });
        }

        const challenge = await Challenges.create({
            title,
            description,
            description_en,
            suggestion,
            suggestion_en,
            name_function,
            params,
            code_temps,
            score,
            rank,
            run_limit_seconds,
            run_limit_memory,
            output_type,
            created_by: req.user._id,
        });

        for (let i = 0; i < test_case.length; i++) {
            await Cases.create({
                challenge_id: challenge._id,
                input: test_case[i].input,
                expect: test_case[i].expect,
                hidden: test_case[i].hidden,
            });
        }

        res.status(200).json({ status: "success", data: challenge });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const updateChallenge = async (req, res) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res
                .status(400)
                .json({ status: "error", error: errors.array() });
        }
        const challenge_id = req.params.id;
        const {
            title,
            description,
            description_en,
            suggestion,
            suggestion_en,
            score,
            rank,
            output_type,
            run_limit_seconds,
            run_limit_memory,
            test_case,
        } = req.body;

        const { name_function } = req.body;
        const { params } = req.body;
        const code_temps = [];

        const getChallenge = await Challenges.findOne({
            _id: challenge_id,
            created_by: req.user._id,
        });

        if (!getChallenge) {
            throw new Error("Challenge not found");
        }

        for (let i = 0; i < languages.length; i++) {
            const { code } = languages[i];
            const codeRender = renderCodeFunction(name_function, params, code);
            code_temps.push({
                lang: code,
                code: codeRender,
            });
        }

        getChallenge.title = title;
        getChallenge.description = description;
        getChallenge.description_en = description_en;
        getChallenge.suggestion = suggestion;
        getChallenge.suggestion_en = suggestion_en;
        getChallenge.name_function = name_function;
        getChallenge.params = params;
        getChallenge.code_temps = code_temps;
        getChallenge.rank = rank;
        getChallenge.output_type = output_type;
        getChallenge.score = score;
        getChallenge.run_limit_seconds = run_limit_seconds;
        getChallenge.run_limit_memory = run_limit_memory;
        await getChallenge.save();

        for (let i = 0; i < test_case.length; i++) {
            const getCase = await Cases.findOne({
                challenge_id: challenge_id,
                _id: test_case[i].id,
            });
            if (!getCase) {
                Cases.create({
                    challenge_id: challenge_id,
                    input: test_case[i].input,
                    expect: test_case[i].expect,
                    hidden: test_case[i].hidden,
                });
            } else {
                getCase.input = test_case[i].input;
                getCase.expect = test_case[i].expect;
                getCase.hidden = test_case[i].hidden;
                await getCase.save();
            }
        }

        res.status(200).json({
            status: "success",
            message: "Update challenge success",
        });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const listChallengeUserCreate = async (req, res) => {
    try {
        const challenges = await Challenges.find({
            created_by: req.user._id,
            type: 0,
        }).sort({ createdAt: -1 })
            .select(
                "title description description_en suggestion suggestion_en rank score"
            )
            .populate("created_by", "_id name avatar");
        res.status(200).json({ status: "success", data: challenges });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const detailChallenge = async (req, res) => {
    try {
        const challenge = await Challenges.findOne({
            _id: req.params.id,
            status: 1,
            type: 0,
        })
            .populate("created_by", "_id name avatar")
            .lean();
        if (!challenge) {
            throw new Error("Challenge not found");
        }

        const cases = await Cases.find({
            challenge_id: challenge._id,
            hidden: false,
        });

        const cases_hidden = await Cases.find({
            challenge_id: challenge._id,
            hidden: true,
        }).select("_id hidden");

        const logChallenges = await LogChallenges.find({
            challenge_id: challenge._id,
            user_id: req.user._id,
        });

        challenge.test_case = cases.concat(cases_hidden);
        challenge.log_challenges = logChallenges;

        res.status(200).json({ status: "success", data: challenge });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const testCaseInChallenge = async (req, res) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res
                .status(400)
                .json({ status: "error", error: errors.array() });
        }
        const { challenge_id } = req.params;
        const { code, language } = req.body;

        const infoChallenge = await Challenges.findOne({
            _id: challenge_id,
            status: 1,
            type: 0,
        });

        if (!infoChallenge) {
            throw new Error("Challenge not found");
        }

        let buff = new Buffer.from(code, "base64");
        let codeDecode = buff.toString("utf8");
        const cases = await Cases.find({ challenge_id, hidden: false });

        const result = await runCode(
            codeDecode,
            language,
            infoChallenge.name_function,
            infoChallenge.run_limit_seconds,
            infoChallenge.run_limit_memory,
            cases,
            infoChallenge.params
        );

        const checkPass = result.find((item) => !item.pass);
        const pass = checkPass ? false : true;

        const getLog = await LogChallenges.findOne({
            challenge_id,
            user_id: req.user._id,
            language: language,
        });
        if (getLog) {
            await LogChallenges.updateOne(
                { _id: getLog._id },
                { $set: { code_text: code } }
            );
        } else {
            await LogChallenges.create({
                challenge_id,
                user_id: req.user._id,
                language,
                code_text: code,
            });
        }

        res.status(200).json({
            status: "success",
            data: { test_case: result, pass },
        });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const submitCaseInChallenge = async (req, res) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res
                .status(400)
                .json({ status: "error", error: errors.array() });
        }

        const { challenge_id } = req.params;
        const { code, language } = req.body;

        const infoChallenge = await Challenges.findOne({
            _id: challenge_id,
            status: 1,
            type: 0,
        });

        if (!infoChallenge) {
            throw new Error("Challenge not found");
        }

        let buff = new Buffer.from(code, "base64");
        let codeDecode = buff.toString("utf8");
        const cases = await Cases.find({ challenge_id });

        const result = await runCode(
            codeDecode,
            language,
            infoChallenge.name_function,
            infoChallenge.run_limit_seconds,
            infoChallenge.run_limit_memory,
            cases,
            infoChallenge.params
        );

        const checkPass = result.find((item) => !item.pass);
        const pass = checkPass ? false : true;
        let score = 0;
        let status = 0;

        const getSubmit = await SubmitChallenges.findOne({
            challenge_id,
            user_id: req.user._id,
        });

        if (pass) {
            score = infoChallenge.score;
            status = 1;
            if (!getSubmit) {
                const infoUser = await User.findOne({ _id: req.user._id });
                infoUser.score += score;
                ranks.map((item) => {
                    if (infoUser.score >= item.min) {
                        infoUser.rank = item.code;
                    }
                });
                await infoUser.save();
            }
        }

        if (getSubmit) {
            await SubmitChallenges.updateOne(
                { _id: getSubmit._id, user_id: req.user._id },
                {
                    $set: {
                        code_text: code,
                        count_submit: getSubmit.count_submit + 1,
                        score,
                        status,
                        language,
                    },
                }
            );
        } else {
            await SubmitChallenges.create({
                challenge_id,
                user_id: req.user._id,
                code_text: code,
                status,
                language,
            });
        }

        res.status(200).json({ status: "success", data: { result, score } });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const challengeJoined = async (req, res) => {
    try {
        // const getLog = await LogChallenges.find({
        //     user_id: req.user._id,
        //     status: 1,
        // });

        const getSubmit = await SubmitChallenges.aggregate([
            {
                $match: {
                    user_id: req.user._id,
                },
            },
            {
                $group: {
                    _id: "$challenge_id",
                    count: { $sum: 1 },
                    status: { $first: "$status" },
                    time_join: { $first: "$createdAt" },
                },
            },
            {
                $lookup: {
                    from: "challenges",
                    localField: "_id",
                    foreignField: "_id",
                    as: "challenge",
                },
            },
            {
                $unwind: "$challenge",
            },
            {
                $project: {
                    name: "$challenge.title",
                    score: "$challenge.score",
                    status: "$status",
                    time_join: "$time_join",
                },
            },
        ])

        res.status(200).json({ status: "success", data: getSubmit });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

export {
    createChallenge,
    updateChallenge,
    listChallengeUserCreate,
    detailChallenge,
    testCaseInChallenge,
    submitCaseInChallenge,
    challengeJoined,
};
