import Contests from "../../models/Contests.js";

const listContest = async (req, res) => {
    try {
        const resultsPerPage = 15;
        let page = Number(req.query.page) >= 1 ? Number(req.query.page) : 1;

        const totalContest = await Contests.find().countDocuments();
        const contests = await Contests.find()
            .select(
                "title avatar time_start time_end description status time_register time_register_end"
            )
            .populate("user_join", "_id name avatar")
            .populate("created_by", "_id name avatar")
            .sort({ time_register_end: "desc" })
            .limit(resultsPerPage)
            .skip(resultsPerPage * (page - 1));
        const totalPages = Math.ceil(totalContest / resultsPerPage);
        const pagination = {
            count: contests.length,
            next_page: page + 1,
            current_page: page,
            total: totalContest,
            total_pages: totalPages,
        };

        res.status(200).json({
            status: "success",
            data: contests,
            pagination,
        });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const registerContest = async (req, res) => {
    try {
        const { contest_id, password } = req.body;
        const contest = await Contests.findById(contest_id);

        if (!contest) {
            throw new Error("Contest not found");
        }

        if (contest.password !== password) {
            throw new Error("Password is incorrect");
        }

        if (contest.time_register > Date.now()) {
            throw new Error("Contest is not available");
        }

        if (contest.time_register_end < Date.now()) {
            throw new Error("Contest is not available");
        }

        const checkUser = contest.user_join.find(
            (user) => user.toString() === req.user._id.toString()
        );
        if (checkUser) {
            throw new Error("You have already registered this contest");
        }

        contest.user_join.push(req.user._id);
        await contest.save();

        res.status(200).json({
            status: "success",
            message: "Join contest successfully",
        });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const getDetailContest = async (req, res) => {
    try {
        const { contest_id } = req.params;
        const contest = await Contests.findById(contest_id)
            .select(
                "title avatar time_start time_end description status time_register time_register_end"
            )
            .populate("created_by", "_id name avatar")
            .populate("user_join", "_id name avatar");

        if (!contest) {
            throw new Error("Contest not found");
        } else {
            res.status(200).json({ status: "success", data: contest });
        }
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};


export { listContest, registerContest, getDetailContest };
