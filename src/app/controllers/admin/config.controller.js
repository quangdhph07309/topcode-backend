import Config from "../../models/Config.js";

const getConfig = async (req, res) => {
    try {
        const config = await Config.findOne();
        res.status(200).json({ status: "success", data: config });
    } catch (err) {
        res.status(400).json({ status: "error", error: err.message });
    }
};

const updateConfig = async (req, res) => {
    try {
        const { title, logo, banner } = req.body;
        const config = await Config.findOne();
        if (!config) {
            await Config.create({
                title,
                logo,
                banner,
            });
        } else {
            config.title = title;
            config.logo = logo;
            config.banner = banner;
            await config.save();
        }
        res.status(200).json({ status: "success", data: config });
    } catch (err) {
        res.status(400).json({ status: "error", error: err.message });
    }
};

export { getConfig, updateConfig };
