import User from "../../models/User.js";

const getListUser = async (req, res, next) => {
    try {
        const resultsPerPage = 15;
        let page = Number(req.query.page) >= 1 ? Number(req.query.page) : 1;

        const totalUser = await User.find().countDocuments();
        const users = await User.find()
            .sort({ name: "asc" })
            .limit(resultsPerPage)
            .skip(resultsPerPage * (page - 1));
        const totalPages = Math.ceil(totalUser / resultsPerPage);
        const pagination = {
            count: users.length,
            next_page: page + 1,
            current_page: page,
            total: totalUser,
            total_pages: totalPages,
        };

        res.status(200).json({ status: "success", data: users, pagination });
    } catch (err) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const getOneUser = async (req, res, next) => {
    try {
        const user = await User.findById(req.params.id).select("-password");
        if (!user) {
            throw new Error("User not found");
        }
        res.status(200).json({ status: "success", data: user });
    } catch (err) {
        res.status(400).json({ status: "error", error: err.message });
    }
};

const updateUser = async (req, res, next) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user) {
            throw new Error("User not found");
        }
        const { name, email, avatar, is_admin } = req.body;
        user.name = name;
        user.email = email;
        user.avatar = avatar;
        user.is_admin = is_admin;
        await user.save();
        res.status(200).json({
            status: "success",
            message: "User updated successfully!",
        });
    } catch (err) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const inActiveUser = async (req, res, next) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user) {
            throw new Error("User not found");
        }
        user.verified = false;
        await user.save();
        res.status(200).json({
            status: "success",
            message: "User updated successfully!",
        });
    } catch (err) {
        res.status(400).json({ status: "error", error: error.message });
    }
};


export { getListUser, updateUser, getOneUser, inActiveUser };
