import Cases from "../../models/Cases.js";
import Challenges from "../../models/Challenges.js";

const getListChallenge = async (req, res) => {
    try {
        const resultsPerPage = 15;
        let page = Number(req.query.page) >= 1 ? Number(req.query.page) : 1;

        const totalChallenges = await Challenges.find({
            type: 0,
        }).countDocuments();
        const challenges = await Challenges.find({
            type: 0,
        })
            .sort({ createdAt: -1 })
            .limit(resultsPerPage)
            .skip(resultsPerPage * (page - 1))
            .select("title name score rank type status created_at")
            .populate("created_by", "_id name avatar");
        const totalPages = Math.ceil(totalChallenges / resultsPerPage);
        const pagination = {
            count: challenges.length,
            next_page: page + 1,
            current_page: page,
            total: totalChallenges,
            total_pages: totalPages,
        };

        res.status(200).json({
            status: "success",
            data: challenges,
            pagination,
        });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const updateChallenge = async (req, res) => {
    try {
        const challenge_id = req.params.id;
        const {
            title,
            description,
            description_en,
            suggestion,
            suggestion_en,
            score,
            rank,
            run_limit_seconds,
            run_limit_memory,
            test_case,
            output_type,
        } = req.body;

        const { name_function } = req.body;
        const { params } = req.body;
        const code_temps = [];

        const getChallenge = await Challenges.findOne({ _id: challenge_id });

        if (!getChallenge) {
            throw new Error("Challenge not found");
        }

        for (let i = 0; i < languages.length; i++) {
            const { code } = languages[i];
            const codeRender = renderCodeFunction(name_function, params, code);
            code_temps.push({
                lang: code,
                code: codeRender,
            });
        }

        getChallenge.title = title;
        getChallenge.description = description;
        getChallenge.description_en = description_en;
        getChallenge.suggestion = suggestion;
        getChallenge.suggestion_en = suggestion_en;
        getChallenge.name_function = name_function;
        getChallenge.params = params;
        getChallenge.code_temps = code_temps;
        getChallenge.score = score;
        getChallenge.rank = rank;
        getChallenge.output_type = output_type;
        getChallenge.run_limit_seconds = run_limit_seconds;
        getChallenge.run_limit_memory = run_limit_memory;
        await getChallenge.save();

        for (let i = 0; i < test_case.length; i++) {
            const getCase = await Cases.findOne({
                challenge_id: challenge_id,
                _id: test_case[i].id,
            });
            if (!getCase) {
                Cases.create({
                    challenge_id: challenge_id,
                    input: test_case[i].input,
                    expect: test_case[i].expect,
                    hidden: test_case[i].hidden,
                });
            } else {
                getCase.input = test_case[i].input;
                getCase.expect = test_case[i].expect;
                getCase.hidden = test_case[i].hidden;
                await getCase.save();
            }
        }

        res.status(200).json({
            status: "success",
            message: "Update challenge success",
        });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const acceptChallenge = async (req, res) => {
    try {
        const challenge_id = req.params.id;
        const getChallenge = await Challenges.findOne({ _id: challenge_id });
        if (!getChallenge) {
            throw new Error("Challenge not found");
        }
        if (getChallenge.status == 1) {
            getChallenge.status = 0;
            await getChallenge.save();
            res.status(200).json({
                status: "success",
                message: "Inactive challenge success",
            });
            return;
        }
        getChallenge.status = 1;
        await getChallenge.save();
        res.status(200).json({
            status: "success",
            message: "Accept challenge success",
        });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

const deleteChallenge = async (req, res) => {
    try {
        const challenge_id = req.params.id;
        const getChallenge = await Challenges.findOne({ _id: challenge_id });
        if (!getChallenge) {
            throw new Error("Challenge not found");
        }
        const getCases = await Cases.find({ challenge_id: challenge_id });
        if (getCases.length > 0) {
            for (let i = 0; i < getCases.length; i++) {
                await getCases[i].remove();
            }
        }

        await getChallenge.remove();
        res.status(200).json({
            status: "success",
            message: "Delete challenge success",
        });
    } catch (error) {
        res.status(400).json({ status: "error", error: error.message });
    }
};

export { getListChallenge, updateChallenge, acceptChallenge, deleteChallenge };
