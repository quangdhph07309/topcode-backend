import jwt from "jsonwebtoken";
import User from "../models/User.js";

const auth = async (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        if (!token) {
            throw new Error();
        }
        const data = jwt.verify(token, process.env.JWT_KEY);

        const user = await User.findOne({
            _id: data._id,
            "tokens.token": token,
        });
        
        if (!user) {
            throw new Error();
        }
        if (!user.verified) {
            return res.status(401).json({
                error: "User inactive!",
            });
        }
        req.user = {
            _id: user._id,
            name: user.name,
            email: user.email,
            is_admin: user.is_admin,
            score: user.score,
        };
        req.token = token;
        next();
    } catch (error) {
        res.status(401).send({
            status: "error",
            error: "Authentication failed!",
        });
    }
};

const authAdmin = async (req, res, next) => {
    try {
        if (!req.user.is_admin) {
            throw new Error("Not authorized to access this resource");
        }

        next();
    } catch (error) {
        res.status(401).send({ status: "error", error: error.message });
    }
};

export { auth, authAdmin };
