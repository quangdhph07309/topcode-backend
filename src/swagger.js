export default {
    openapi: "3.0.1",
    info: {
        version: "1.0.0",
        title: "APIs Document",
        description: "API Document Web Application",
        termsOfService: "",
        contact: {
            name: "MokaDEV",
            email: "tuhd4@fpt.edu.vn",
            url: "https://mokadev.asia",
        },
        license: {
            name: "Apache 2.0",
            url: "https://www.apache.org/licenses/LICENSE-2.0.html",
        },
    },
    servers: [
        {
            url: "http://{ip}:{port}/{basePath}",
            description: "The production API server",
            variables: {
                ip: {
                    enum: ["api.topcode.asia", "localhost"],
                },
                port: {
                    enum: ["", "3000"],
                    default: "",
                },
                basePath: {
                    enum: ["", "admin", "user"],
                    default: "",
                },
            },
        },
    ],
    components: {
        securitySchemes: {
            jwt: {
                type: "http",
                scheme: "bearer",
                in: "header",
                bearerFormat: "JWT",
            },
        },
    },
    paths: {
        "/login": {
            post: {
                tags: ["Auth"],
                summary: "Login",
                description: "Login",
                operationId: "login",
                consumes: ["application/json"],
                produces: ["application/json"],
                requestBody: {
                    content: {
                        "application/json": {
                            schema: {
                                type: "object",
                                properties: {
                                    email: {
                                        type: "string",
                                        default: "tuhoangx@gmail.com",
                                        required: true,
                                    },
                                    password: {
                                        type: "string",
                                        default: "12345678",
                                        required: true,
                                    },
                                },
                            },
                        },
                    },
                },
                responses: {
                    200: {
                        description: "Success",
                    },
                    400: {
                        description: "Bad Request",
                    },
                    401: {
                        description: "Unauthorized",
                    },
                },
            },
        },
        "/register": {
            post: {
                tags: ["Auth"],
                summary: "register",
                description: "register",
                operationId: "register",
                consumes: ["application/json"],
                produces: ["application/json"],
                requestBody: {
                    content: {
                        "application/json": {
                            schema: {
                                type: "object",
                                properties: {
                                    name: {
                                        type: "string",
                                        default: "Hoàng Duy Tú",
                                        required: true,
                                    },
                                    email: {
                                        type: "string",
                                        default: "tuhoangx@gmail.com",
                                        required: true,
                                    },
                                    password: {
                                        type: "string",
                                        default: "12345678",
                                        required: true,
                                    },
                                },
                            },
                        },
                    },
                },
                responses: {
                    200: {
                        description: "Success",
                    },
                    400: {
                        description: "Bad Request",
                    },
                    401: {
                        description: "Unauthorized",
                    },
                },
            },
        },

        "/profile": {
            post: {
                tags: ["User"],
                summary: "Get profile",
                description: "Get profile",
                operationId: "get-profile",
                consumes: ["application/json"],
                produces: ["application/json"],
                responses: {
                    200: {
                        description: "Success",
                    },
                    400: {
                        description: "Bad Request",
                    },
                    401: {
                        description: "Unauthorized",
                    },
                },
                security: [
                    {
                        jwt: [],
                    },
                ],
            },
        },
        "/update-profile": {
            post: {
                tags: ["User"],
                summary: "Update profile",
                description: "Update profile",
                operationId: "get-profile",
                consumes: ["application/json"],
                produces: ["application/json"],
                requestBody: {
                    content: {
                        "application/json": {
                            schema: {
                                type: "object",
                                properties: {
                                    name: {
                                        type: "string",
                                        default: "New name",
                                    },
                                    avatar: {
                                        type: "string",
                                        default:
                                            "https://avatars0.githubusercontent.com/u/17098981?s=460&v=4",
                                    },
                                    birthday: {
                                        type: "date",
                                        default:
                                            "2021-10-20T15:57:56.970+00:00",
                                    },
                                    sex: {
                                        type: "number",
                                        default: 1,
                                    },
                                },
                            },
                        },
                    },
                },
                responses: {
                    200: {
                        description: "Success",
                    },
                    400: {
                        description: "Bad Request",
                    },
                    401: {
                        description: "Unauthorized",
                    },
                },
                security: [
                    {
                        jwt: [],
                    },
                ],
            },
        },
        "/show-course": {
            post: {
                tags: ["User"],
                summary: "Show course ",
                description: "Show course ",
                operationId: "show-course",
                consumes: ["application/json"],
                produces: ["application/json"],
                responses: {
                    200: {
                        description: "Success",
                    },
                    400: {
                        description: "Bad Request",
                    },
                    401: {
                        description: "Unauthorized",
                    },
                },
                security: [
                    {
                        jwt: [],
                    },
                ],
            },
        },
    },
};
