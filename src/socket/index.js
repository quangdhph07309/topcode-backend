import { Server } from "socket.io";

const socket = (server) => {
    const io = new Server(server);
    io.on("connection", (sk) => {
        console.log("a user connected");
    });
};

export default socket;
